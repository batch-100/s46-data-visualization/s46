import React from "react";
import { Doughnut } from "react-chartjs-2";

function DoughnutChart({ criticals, deaths, recoveries }) {
  return (
    <Doughnut
      data={{
        datasets: [
          {
            data: [criticals, deaths, recoveries],
            backgroundColor: ["red", "black", "green"],
          },
        ],
        labels: ["Criticals", "Deaths", "Recoveries"],
      }}
      redraw={false}
    />
  );
}

export default DoughnutChart;
