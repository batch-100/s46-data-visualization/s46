import Head from "next/head";
import { Jumbotron } from "react-bootstrap";
import toNum from "../helpers/toNum";

import styles from "../styles/Home.module.css";

export default function Home({ globalTotal }) {
  console.log(globalTotal);
  return (
    <>
      <Jumbotron>
        <h1 className="text-left">Covid Tracker</h1>
        <h4>
          Total Covid-19 cases in the world:{" "}
          <strong>{globalTotal.cases}</strong>
        </h4>
      </Jumbotron>
    </>
  );
}
//export async function getStaticProps ()
export async function getServerSideProps() {
  const res = await fetch(
    "https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php",
    {
      method: "GET",
      headers: {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      },
      //when the data is fetch
      //when ibu-build
    }
  );
  const data = await res.json();
  const countriesStats = data.countries_stat;

  let total = 0;
  countriesStats.forEach(country => {
    total += toNum(country.cases);
  });
  const globalTotal = {
    cases: total,
  };
  return {
    props: {
      globalTotal,
    },
  };
}
