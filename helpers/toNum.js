export default function toNum(str) {
    //converts the string to an array and to also gain acces to array to array methods
    const arr = [...str];
    //["6", ","8","4"]
    //filter out the commas in the string
    const filteredArr = arr.filter(element => element !== ',');
    //["6","8",...]
    //reduce the filtered array back to a single string without commas
    return parseInt(filteredArr.reduce((x,y) => x + y));
    //"6846010" using the parseInt => 6846010
}
